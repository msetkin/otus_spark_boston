package com.github.mrpowers.my.cool.project

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql._
import org.apache.spark.sql.functions.broadcast
import org.apache.spark.sql.functions
import org.apache.spark.sql.functions._
import org.apache.spark.sql._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{callUDF, lit, concat}

object BostonAnalysis extends App {
  def getRankedValue = udf(
    (actualRank: Int, requiredRank: Int, category: String)=>{
      if (actualRank == requiredRank) category else ""
    }
  )

  if (args.length < 3) {
    println("dude, i need three parameters")
  }

  val fname_path_to_crime = "data/crime.csv"
  val fname_path_to_offense_codes = "data/offense_codes.csv"
  val path_to_output = "data/output"

  if (args.length == 0) {
    println (s"path_to_crime is set to $fname_path_to_crime")
    println (s"path_to_offense_codes is set to $fname_path_to_offense_codes")
    println (s"path_to_output is set to $path_to_output")
  } else
  if (args.length == 1) {
    val fname_path_to_crime = args(0)
    println (s"path_to_offense_codes is set to $fname_path_to_offense_codes")
    println (s"path_to_output is set to $path_to_output")
  } else
  if (args.length == 2) {
    val fname_path_to_crime = args(0)
    val fname_path_to_offense_codes = args(1)
    println (s"path_to_output is set to $path_to_output")
  } else
  {
    val fname_path_to_crime = args(0)
    val fname_path_to_offense_codes = args(1)
    val path_to_output = args(2)
  }

  val spark = SparkSession
    .builder()
    .master("local[*]")
    .appName("spark session")
    .getOrCreate()

  import spark.implicits._

  val crimeFacts = spark
    .read
    .option("header", "true")
    .option("inferSchema", "true")
    .csv(fname_path_to_crime)
    .na.fill("-1",Array("DISTRICT"))

  crimeFacts.cache()

  val offenseCodes = spark
    .read
    .option("header", "true")
    .option("inferSchema", "true")
    .csv(fname_path_to_offense_codes)
    .withColumn("rank",
      row_number().over(Window
        .partitionBy($"CODE")
        .orderBy($"NAME")
      )
    )
    .where($"rank" === 1)

  val offenseCodesBroadcast = broadcast(offenseCodes)

  val frequent_crime_types_df = crimeFacts.as("cf")
    .join(offenseCodesBroadcast.as("oc"), $"cf.OFFENSE_CODE" === $"oc.CODE")
    .withColumn("base_crime_type",
      split($"oc.NAME", " - "){0}
    )
    .groupBy($"cf.DISTRICT", $"base_crime_type")
    .agg(
      functions.count("cf.INCIDENT_NUMBER").as("crimes_total")
    )
    .withColumn("crime_type_order",
      rank().over(Window
        .partitionBy($"cf.DISTRICT")
        .orderBy($"crimes_total".desc)
      )
    )
    .withColumnRenamed("cf.DISTRICT", "district")
    .select ("cf.DISTRICT", "base_crime_type", "crimes_total", "crime_type_order")
    .filter( $"crime_type_order" < 4)
    .groupBy($"cf.DISTRICT")
    .agg(
      functions.max(getRankedValue($"crime_type_order".cast("integer"),
        lit(1),
        $"base_crime_type".cast("string"))).as("crime_type_1"),
      functions.max(getRankedValue($"crime_type_order".cast("integer"),
        lit(2),
        $"base_crime_type".cast("string"))).as("crime_type_2"),
      functions.max(getRankedValue($"crime_type_order".cast("integer"),
        lit(3),
        $"base_crime_type".cast("string"))).as("crime_type_3")
    )
    .withColumn("frequent_crime_types",
      concat($"crime_type_1", lit(", "), $"crime_type_2", lit(", "), $"crime_type_3")
    )
    .select ("DISTRICT", "frequent_crime_types")


  val frequent_crime_types_df_bc = broadcast(frequent_crime_types_df)

  val robberyStatsWithBroadcast = crimeFacts.as("cf")

    .join(frequent_crime_types_df_bc.as("fct"), $"cf.DISTRICT" === $"fct.DISTRICT")
    .withColumn("crimes_monthly",
      count($"cf.INCIDENT_NUMBER") over Window.partitionBy($"cf.YEAR", $"cf.MONTH", $"cf.DISTRICT")
    )
    .groupBy("cf.DISTRICT", "fct.frequent_crime_types")
    .agg(
      functions.count("cf.INCIDENT_NUMBER").as("crimes_total"),
      functions.mean("cf.Lat").as("lat"),
      functions.mean("cf.Long").as("lng"),
      callUDF("percentile_approx", $"crimes_monthly", lit(0.5)).as("crimes_monthly")
    )
    .withColumnRenamed("DISTRICT", "district")
    .withColumn("district", when($"district" === "-1", null).otherwise($"district"))
    .select ($"district", $"crimes_total", $"crimes_monthly", $"fct.frequent_crime_types", $"lat", $"lng")
    .repartition(1)
    .write
    .mode(SaveMode.Overwrite)
    .parquet(path_to_output)
}